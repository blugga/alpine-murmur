FROM alpine:latest

RUN apk add --update curl

ENV VERSION 1.2.16

RUN curl -LO https://github.com/mumble-voip/mumble/releases/download/$VERSION/murmur-static_x86-$VERSION.tar.bz2 \
    && mkdir /murmur/ \
    && tar -xjf murmur-static_x86-$VERSION.tar.bz2 -C /murmur/

RUN mkdir -p /var/run/murmurd \
    && mkdir -p /etc/murmurd \
    && mv /murmur/murmur-static_x86-$VERSION/murmur.ini /etc/murmurd/ 

WORKDIR /murmur/murmur-static_x86-$VERSION
CMD ["./murmur.x86", "-fg", "-v", "-ini", "/etc/murmurd/murmur.ini"] 
